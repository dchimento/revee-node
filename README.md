* Revee Docker Node

  This creates a docker image from the offical docker node repo
  https://hub.docker.com/_/node/
  It adds the private npm repo for use of npm install.
  Node Version: 4.6 (See Dockerfile)
  You need to have the artifact username/password to build and or deploy


* Building Locally
./build.sh

